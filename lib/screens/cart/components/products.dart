import 'package:flutter/material.dart';
import 'package:food_delivery/constants.dart';

enum ProductSize { P, M, G }

class Product extends StatelessWidget {
  final String image;
  final String name;
  final double price;
  final String weight;
  final int quantity;
  final ProductSize size;

  const Product({
    required this.image,
    required this.name,
    required this.price,
    required this.weight,
    required this.quantity,
    required this.size,
    Key? key
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    List<String> sizes = ['P', 'M', 'G'];

    return Container(
      margin: const EdgeInsets.only(top: 11.0, left: 12.0, right: 22.0, bottom: 11.0),
      child: Row(
        children: [
          Container(
            width: 87,
            height: 80,
            decoration: BoxDecoration(
              image: DecorationImage(
                alignment: Alignment.center,
                fit: BoxFit.cover,
                image: AssetImage(this.image),
              )
            ),
            child: Stack(
              children: [
                Positioned(
                  top: 0.0,
                  right: 0.0,
                  child: Container(
                    width: 25.0,
                    height: 25.0,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: ThemeColor.grey.withOpacity(0.75),
                      borderRadius: BorderRadius.circular(30.0)
                    ),
                    child: Text(this.quantity.toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600))
                  ),
                ),
              ],
            )
          ),
          SizedBox(width: 6.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(this.name, style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: ThemeColor.grey)),
                SizedBox(height: 5.0),
                Text('${this.weight} - ${sizes[this.size.index]}', style: TextStyle(fontSize: 13, color: ThemeColor.greyVariant)),
              ],
            )
          ),

          RichText(
              text: TextSpan(
                text: '\$',
                style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.w500, color: ThemeColor.primary),
                children: <TextSpan>[
                  TextSpan(
                    text: this.price.toStringAsFixed(2).replaceAll('.00', ''),
                    style: TextStyle(fontSize: 23.0, color: ThemeColor.grey),
                  )
                ]
              ),
            )
        ],
      ),
    );
  }
}


class Products extends StatefulWidget {
  final bool show;
  const Products({ this.show = false, Key? key }) : super(key: key);

  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  bool topShow = false;

  final ScrollController _productsScrollController = new ScrollController();

  @override
  void initState() {
    super.initState();

    _productsScrollController.addListener(() {
      print(_productsScrollController.offset);

      if (_productsScrollController.offset > 2.0) {
        setState(() {
          this.topShow = true;
        });
      } else {
        setState(() {
          this.topShow = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 900),
        curve: Curves.easeInQuad,
        transform: Matrix4.translationValues(0, widget.show ? 0 : 500, 0),
        child: Container(
          width: double.infinity,
          transform: Matrix4.translationValues(0, -20.0, 0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))
          ),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 1,
                    child: ListView(
                      controller: _productsScrollController,
                      children: [
                        Product(image: 'images/burger.png', name: 'Beef Burger', quantity: 1, price: 25.0, weight: '250g', size: ProductSize.G),
                        Product(image: 'images/hot-wings.png', name: 'Hot Wings', quantity: 1, price: 10.0, weight: '300g', size: ProductSize.M),
                        Product(image: 'images/coca-cola.png', name: 'Coca Cola', quantity: 2, price: 2.50, weight: '250ml', size: ProductSize.P)
                      ],
                    ),
                  ),

                  Stack(children: [
                    Transform(
                      transform: Matrix4.translationValues(0, -50.0, 0),
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.white10,
                              Colors.white
                            ],
                            begin: Alignment.center,
                            end: Alignment.bottomCenter,
                          )
                        ),
                      ),
                    ),
                    // Payment method
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, left: 22.0, right: 22.0, bottom: 25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Payment method', style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                            color: ThemeColor.grey,
                          )),

                          Row(
                            children: [
                              Container(
                                width: 32.26,
                                height: 25.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('images/mastercard-logo.png')
                                  )
                                )
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                child: Text('**** **** **** 3095', style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.w600,
                                  color: ThemeColor.greyVariant,
                                )),
                              ),
                              OutlinedButton(
                                onPressed: () => print("Mudar cartão"),
                                child: Text('Change', style: TextStyle(color: ThemeColor.primary)),
                                style: OutlinedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  side: BorderSide(width: 2, color: ThemeColor.greyVariant.withOpacity(0.1))
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 30.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    text: TextSpan(
                                      text: 'Total: ',
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: '\$',
                                          style: TextStyle(
                                            fontSize: 19.0,
                                          )
                                        ),
                                        TextSpan(
                                          text: '37,50',
                                          style: TextStyle(
                                            fontSize: 23.0
                                          )
                                        ),
                                      ],
                                      style: TextStyle(
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.w700,
                                        color: ThemeColor.grey,
                                      )
                                    )
                                  ),

                                  SizedBox(height: 5.0),
                                  Text('4 items', style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.w600,
                                    color: ThemeColor.greyVariant
                                  ))
                                ],
                              ),
                              SizedBox(width: 35.0),
                              Expanded(
                                child: ElevatedButton(
                                  onPressed: () => print('Paying...'),
                                  child: Text('Pay', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600)),
                                  style: ElevatedButton.styleFrom(
                                    padding: EdgeInsets.symmetric(vertical: 17.0),
                                    primary: ThemeColor.primary,
                                    elevation: 10.0,
                                    shadowColor: ThemeColor.primary.withOpacity(0.25),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ]),
                ],
              ),
              Positioned(
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 200),
                  opacity: (topShow ? 1.0 : 0.0),
                  child: Container(
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
                      gradient: LinearGradient(
                        colors: [
                          Colors.white70,
                          Colors.white10
                        ],
                        begin: Alignment.center,
                        end: Alignment.bottomCenter,
                      )
                    ),
                  ),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}